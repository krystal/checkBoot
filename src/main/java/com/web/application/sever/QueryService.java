package com.web.application.sever;

import java.util.Map;

  

public interface QueryService {
	  //账单列表
	  Object payList(Map<String, Object> param);
	  //所有明细 包含(各种状态，主子或者只有主)
	  Object payListInfo(Map<String, Object> param);
      //对应 合同
	  Object payCert(Map<String, Object> param);
	  //插入合同
	  Object insertCert(Map<String, Object> param);
} 
