package com.web.application.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class PropertyHelper {
	//反转List<Map>
	
	public List<Map<String,Object>> lowerCaseForList(List<Map<String,Object>> upCase){
		List<Map<String,Object>> dataL=new ArrayList<Map<String,Object>>();
		for(Map<String,Object> map : upCase) {
			Map<String,Object> lower=new HashMap<String,Object>();
			for(String key:map.keySet()) {
				lower.put(key.toLowerCase(), map.get(key));
			}
			dataL.add(lower);
		}
		return dataL;
	}
	
	//反转Map
	 
	public  Map<String,Object> lowerCaseForMap(Map<String,Object> upCase){
		Map<String,Object> dataM=new HashMap<String,Object>();
		for(String key:upCase.keySet()) {
			dataM.put(key.toLowerCase(), upCase.get(key));
		}
		return dataM;
	}
	
	//反转Map<Map>
	
	public  Map<String,Map<String,Object>> lowerCaseForMapM(Map<String,Map<String,Object>> upCase){
		Map<String,Map<String,Object>> dataMM=new HashMap<String,Map<String,Object>>();
		for(String keyM:upCase.keySet()) {
			Map<String,Object> ds=new HashMap<String,Object>();
				for(String key:upCase.get(keyM).keySet()) {
					ds.put(key, upCase.get(keyM).get(key));
				}
			dataMM.put(keyM.toLowerCase(), ds);	
		}
		return dataMM;
	}
}
