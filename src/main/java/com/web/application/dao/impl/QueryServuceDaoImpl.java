package com.web.application.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.web.application.dao.QueryServiceDao;

@Repository
public class QueryServuceDaoImpl implements QueryServiceDao{
	@Autowired
    @Qualifier("jdbcTempSencondary")
    private NamedParameterJdbcTemplate jdbcTemplate;
	
	
	// 初始化列表 改成 1,2,3 初始化 处理中 已完成   ,当 staus为 0时 查1,2 其他的照常
	public List<Map<String, Object>> queryPayList(String code, int state) {
		Map<String,Object> condition=new HashMap<String,Object>();
		condition.put("code", code);
		condition.put("state", state);
		if(state==0) {
			String sql="select * from bank_rec where status in ('1','2')";
			List<Map<String,Object>> payList=jdbcTemplate.queryForList(sql,condition);
			return payList;
		}
		String sql="select * from bank_rec where  status=:state";
 		List<Map<String,Object>> payList=jdbcTemplate.queryForList(sql,condition);
		return payList;
 	}

	@Override
	public Map<String,Object > queryPayListInfo(String pk_pay) {
 		Map<String,Object> condition=new HashMap<String,Object>();
		condition.put("pk_pay", pk_pay);		 
		String sql="select * from bank_rec where pk_pay=:pk_pay";
		Map<String,Object> master=jdbcTemplate.queryForMap(sql.toString(),condition);
 		return master;
	}

	@Override
	public Map<String, Map<String, Object>> queryPayListInfoed(String pk_pay) {
		Map<String,Map<String,Object>> payListInfoed=new HashMap<String,Map<String,Object>>();
		
		payListInfoed.put("master", queryPayListInfo(pk_pay));//插入主表
 		
		Map<String,Object> condition=new HashMap<String,Object>();
 		condition.put("pk_pay", "~");
 		String certSql="select * from t_cert where pk_pay=:pk_pay"; // 略略 还没定
 		List<Map<String,Object>> certList=jdbcTemplate.queryForList(certSql,condition);
 		
 		for(Map<String,Object> map:certList) {
 			payListInfoed.put((String)map.get("pk_cert"), map);
 		}
 		return payListInfoed;
	}

	@Override
	public List<Map<String, Object>> queryPayCert(String userId) {
		Map<String,Object> condition=new HashMap<String,Object>();
		String sql="select * from t_cert where pk_pay=:pk_pay";
		condition.put("pk_pay", "~");		
		List<Map<String,Object>> payCert=jdbcTemplate.queryForList(sql, condition);
 		return payCert;
	}

	@Override
	public Map<String, Object> insertCert(String pk_cert) {
		// TODO Auto-generated method stub
		return null;
	}
}
